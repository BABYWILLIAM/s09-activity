package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//Will require all routes within the "Discussion Application" to use "/greeting" as part of its courses
@RequestMapping("/greeting")
//The "@RestController" annotation tells Spring Boot that this application will function as endpoint that will be used in handling web requests
public class DiscussionApplication {


	public static void main(String[] args) {SpringApplication.run(DiscussionApplication.class, args);}


	@GetMapping("/hello")
	// Maps a get request to the route "/hello" and the method "hello()"
	public String hello () {
		return  "Hello World!";
	}

	// Routes with a string query
	// Dynamic data is obtained from the URL's string query
	//http://localhost:8080/hello
	//http://localhost:8080/hi?name=William
	//"%s" specifies that the value to be included in the format is of any data type
	@GetMapping("/hi")
	public String hi (@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	// Multiple Parameters
	//http://localhost:8080/friend
	//http://localhost:8080/friend?name=william&friend=germs
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	// Routes with path variables
	@GetMapping("/hello/{name}")
	// localhost:8080/hello/joe
	// @PathVariable annotation allows us to extract data directly from the URL.
	public String courses (@PathVariable ("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}



// S09 ACTIVITY
//http://localhost:8080/greeting/enroll?user=william
//http://localhost:8080/greeting/getEnrollees
//http://localhost:8080/greeting/nameage?name=William&age=22
//http://localhost:8080/greeting/courses/java101
	ArrayList enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam String user) {
		enrollees.add(user);
		return "Thank you for enrolling, " + user + "!";
	}

	@GetMapping("/getEnrollees")
	public ArrayList getEnrollees(){
		return enrollees;
	}


	@GetMapping("/nameage")
	public String nameAge(@RequestParam String name, @RequestParam int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	@GetMapping("/courses/{id}")
	public String courseId(@PathVariable ("id") String id) {
		String message;
		if(id.equals("java101")){
			message = "Name: Java101, Schedule: MWF 10:30 AM - 12:30 PM, Price: PHP 3500";
		}
		else if(id.equals("sql101")){
			message = "Name: SQL101, Schedule: TTH 12:30 PM - 3:30 PM, Price: PHP 3250";
		}
		else if(id.equals("javaee101")){
			message = "Name: JavaEE101, Schedule: MWF 9:00 AM - 12:00 PM, Price: PHP 3100";
		}
		else{
			message = "Course cannot be found!";
		}

		return message;
	}

}

